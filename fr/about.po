# translation of about.po to French
# Translation of Debian release notes to French
# Copyright (C) 2005-2011 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian release notes.
#
# Translators:
# Denis Barbier, -2004
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008-2009.
# Frédéric Bothamy <frederic.bothamy@free.fr>, 2004-2007.
# Christian Perrier <bubulle@debian.org>, 2009, 2013.
# Thomas Blein <tblein@tblein.eu>, 2011, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: about\n"
"POT-Creation-Date: 2019-05-14 11:27+0200\n"
"PO-Revision-Date: 2017-06-04 21:49+0100\n"
"Last-Translator: Thomas Blein <tblein@tblein.eu>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "fr"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introduction"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Ce document présente aux utilisateurs de la distribution &debian; les "
"changements majeurs introduits dans la version &release; (nom de code "
"&Releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Les notes de publication fournissent des informations sur la façon "
"d'effectuer une mise à niveau depuis la version précédente &oldrelease; (nom "
"de code &Oldreleasename;) vers la version actuelle et renseignent les "
"utilisateurs sur les problèmes éventuels qu'ils pourraient rencontrer "
"pendant cette mise à niveau."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"La version la plus récente de ce document est toujours disponible à "
"l'adresse <ulink url=\"&url-release-notes;\"></ulink>. En cas de doute, "
"vérifiez la date du document en première page et assurez-vous de lire la "
"dernière version."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Veuillez noter qu'il est impossible de lister tous les problèmes connus. "
"C'est pourquoi une sélection a été faite selon la fréquence et l'impact de "
"ces problèmes."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Veuillez noter que nous ne prenons en charge et documentons que les mises à "
"niveau depuis la précédente version de Debian (dans ce cas, la mise à niveau "
"depuis &Oldreleasename;). Si vous devez effectuer la mise à niveau depuis "
"une version antérieure, nous vous suggérons de lire les éditions précédentes "
"de ces notes de publication et de commencer par faire une mise à niveau vers "
"&Oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Signaler des bogues au sujet de ce document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Nous avons essayé de tester toutes les différentes étapes de mise à niveau "
"décrites dans ce document, en essayant d'anticiper tous les problèmes que "
"peuvent rencontrer nos utilisateurs."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Cependant, si vous pensez avoir trouvé un bogue dans cette documentation "
"(une information incorrecte ou manquante), merci de soumettre un rapport de "
"bogue dans le <ulink url=\"&url-bts;\">système de suivi des bogues</ulink> "
"sur le pseudo-paquet <systemitem role=\"package\">release-notes</"
"systemitem>. Pensez à consulter au préalable les <ulink url=\"&url-bts-rn;"
"\">rapports de bogue existants</ulink> pour vérifier que ce problème n'a pas "
"déjà été signalé. N'hésitez pas à ajouter des informations supplémentaires "
"aux rapports de bogue existants si vous pouvez contribuer au contenu de ce "
"document."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Nous apprécions, et encourageons, les rapports qui fournissent des "
"correctifs aux sources du document. Vous pouvez trouver plus de "
"renseignements sur la manière d'obtenir les sources de ce document en <xref "
"linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Fournir des comptes-rendus de mise à niveau"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Nous recueillons toutes les expériences de nos utilisateurs sur les mises à "
"niveau de &Oldreleasename; vers &Releasename;. Si vous désirez partager la "
"vôtre, veuillez soumettre un rapport de bogue dans le <ulink url=\"&url-bts;"
"\">système de suivi des bogues</ulink> sur le pseudo-paquet <systemitem role="
"\"package\">upgrade-reports</systemitem> présentant votre bilan. Nous vous "
"demandons de compresser toutes les pièces jointes (en utilisant "
"<command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Veuillez fournir les renseignements suivants lors de l'envoi de votre compte-"
"rendu de mise à niveau :"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"l'état de votre base de données de paquets avant et après la mise à niveau : "
"la base de données d'état de <systemitem role=\"package\">dpkg</systemitem> "
"disponible dans <filename>/var/lib/dpkg/status</filename> et les "
"informations d'état des paquets d'<systemitem role=\"package\">apt</"
"systemitem> disponibles dans <filename>/var/lib/apt/extended_states</"
"filename>. Vous devriez faire une sauvegarde avant la mise à niveau comme "
"décrit en <xref linkend=\"data-backup\"/>, mais vous pouvez également "
"trouver des sauvegardes de <filename>/var/lib/dpkg/status</filename> dans "
"<filename>/var/backups</filename> ;"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"les fichiers journaux de session créés avec <command>script</command>, comme "
"décrit en <xref linkend=\"record-session\"/> ;"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"vos fichiers journaux d'<systemitem role=\"package\">apt</systemitem>, "
"disponibles dans <filename>/var/log/apt/term.log</filename>, ou ceux "
"d'<command>aptitude</command>, disponibles dans <filename>/var/log/aptitude</"
"filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Prenez le temps de parcourir les journaux et d'en supprimer toute "
"information sensible ou confidentielle avant de les inclure dans un rapport "
"de bogue car ces informations seront publiées dans une base de données "
"publique."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Sources de ce document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"Ce document utilise le format DocBook XML<indexterm><primary>DocBook XML</"
"primary></indexterm>. La version HTML est créée avec <systemitem role="
"\"package\">docbook-xsl</systemitem> et <systemitem role=\"package"
"\">xsltproc</systemitem>. La version PDF est créée avec <systemitem role="
"\"package\">dblatex</systemitem> ou <systemitem role=\"package\">xmlroff</"
"systemitem>. Les sources des notes de publication sont disponibles dans le "
"dépôt Git du <emphasis>Projet de documentation Debian </emphasis>. Vous "
"pouvez utiliser l'<ulink url=\"&url-vcs-release-notes;\">interface web</"
"ulink> pour accéder aux fichiers par le web et pour consulter leurs "
"modifications. Veuillez consulter les pages du <ulink url=\"&url-ddp-vcs-"
"info;\">dépôt Git du projet de documentation Debian</ulink> pour obtenir "
"plus de renseignements sur les accès au dépôt Git."

#~ msgid "TODO: any more things to add here?\n"
#~ msgstr "A FAIRE: quelque chose à ajouter ici?\n"
